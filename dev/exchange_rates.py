import sys
from datetime import datetime, timedelta
import pandas as pd
import http.client
import mimetypes
from datetime import datetime, timedelta
from ast import literal_eval
import requests
import json
import pytz
import sys
from bson.objectid import ObjectId
import incentivedkutils as utils
from gcp_sql import mysql

def main():
    return

def apply_exchange_rates(price_dict, exrates):

    exrate_EUR = float(exrates['EUR'])/float(exrates[price_dict['product_currency']])
    exrate_USD = float(exrates['USD'])/float(exrates[price_dict['product_currency']])

    price_dict_local = {k:v for k, v in sorted(price_dict.items())}
    price_dict_USD = {k:v*exrate_USD for k, v in price_dict.items() if 'price' in k}
    price_dict_EUR = {k:v*exrate_EUR for k, v in price_dict.items() if 'price' in k}

    price_dict_USD['product_currency'] = 'USD_USD'
    price_dict_EUR['product_currency'] = 'EUR_EUR'
    price_dict_local['product_currency'] = price_dict_local['product_currency'] + '_Local'

    price_dict = {k:v for k,v in price_dict.items() if k not in price_dict_USD.keys()}

    price_dict_USD.update(price_dict)
    price_dict_EUR.update(price_dict)
    price_dict_USD = {k:v for k,v in sorted(price_dict_USD.items())}
    price_dict_EUR = {k:v for k,v in sorted(price_dict_EUR.items())}
    return price_dict_local, price_dict_EUR, price_dict_USD


def retrieve_exchange_rates():
    exchange_rates = mysql('exchange_rates').read_data(f'exchange_rates')
    print('Date of exchange rates are:', exchange_rates[0]['date'].strftime("%m/%d/%Y"))

    currency_dict = {}
    for dict in exchange_rates:
        currency_dict.update({dict['currency']:dict['base_usd']})
    return currency_dict

if __name__ == '__main__':
    main()
