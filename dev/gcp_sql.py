import pyodbc
import pymysql as mc
import math
import sys

class _sql_connector():
    def __init__(self):
        self.convert_none_and_nan_to_null([{}])

    @property
    def connection(self):
        return self._conn

    @property
    def cursor(self):
        return self._cursor

    def commit(self):
        self.connection.commit()

    def close(self):
        self.cursor.close()

    def create_table(self, table_name, varlist):
        self.cursor.execute("SHOW TABLES")
        tables = [x[0] for x in list(self.cursor)]
        if table_name not in tables:
            self.cursor.execute(f"CREATE TABLE {table_name} {varlist}")

    def drop_table(self, table_name):
        try:
            self.cursor.execute(f"DROP TABLE {table_name}")
        except:
            pass

    def clear_table(self, table_name):
        self.cursor.execute(f'TRUNCATE TABLE {table_name}')

    def list_tables(self):
        self.cursor.execute("SHOW TABLES")
        return [x[0] for x in list(self.cursor)]

    def read_data(self, table_name, conditions=''):
        self.cursor.execute(f"SHOW COLUMNS FROM {table_name}")
        varnames = [x[0] for x in list(self.cursor)]
        if len(conditions):
            conditions = {k: f'"{v}"' for k, v in conditions.items()}
            cond_list = ' AND '.join([''.join([obs, ' = ', str(conditions[obs])]) for obs in conditions])
            self.cursor.execute(f"SELECT * FROM {table_name} WHERE {cond_list}")
        else:
            self.cursor.execute(f"SELECT * FROM {table_name}")
        in_list = [c for c in list(self.cursor)]
        data_list = [dict(zip(varnames, obs)) for obs in in_list]
        return data_list

    def insert_data(self, table_name, list_of_dicts):

        list_of_dicts = self.convert_non_and_nan_to_null(list_of_dicts)

        batch_size = 999
        batches = len(list_of_dicts) // batch_size + 1

        # print('batch size: ',batches)


        for batch in range(batches):
            values_tuples = [tuple([v for v in obs.values()]) for obs in
                             list_of_dicts[batch * batch_size:batch_size * (batch + 1)]]
            values = ', '.join(map(str, values_tuples))
            values = values.replace("'NULL'", "NULL")

            self.cursor.execute(f'INSERT INTO {table_name} VALUES {values}')
        self.commit()

    def convert_non_and_nan_to_null(self, list_of_dicts):
        if len(list_of_dicts):
            dict_keys = list_of_dicts[0].keys()
            for record in list_of_dicts:
                for key in dict_keys:
                    # account for numerics
                    try:
                        replace = math.isnan(record[key])
                    except:
                        replace = False

                    # account for strings
                    if record[key] == None:
                        replace = True
                    if replace:
                        record[key] = 'NULL'
        return list_of_dicts


class sqlserver(_sql_connector):
    def __init__(self,db_name,server='dev'):
        if server=='prod':
            conn_str = "DRIVER={ODBC Driver 17 for SQL Server};SERVER=35.228.16.15;UID=torben;PWD=Incentive1"
        else:
            conn_str="DRIVER={ODBC Driver 17 for SQL Server};SERVER=35.228.16.15;UID=torben;PWD=Incentive1"
        self._conn = pyodbc.connect(conn_str, autocommit=True)
        self._cursor = self._conn.cursor()
        csr=self.cursor.execute('SELECT name, database_id, create_date FROM sys.databases')
        if db_name not in [x[0] for x in list(csr)]:
            self.cursor.execute(f'CREATE DATABASE {db_name}')
        self.cursor.execute(f'USE {db_name}')

    def create_table(self, table_name,varlist):
        self.cursor.execute("SELECT * FROM  SYSOBJECTS WHERE xtype = 'U'")
        tables = [x[0] for x in list(self.cursor)]
        if table_name not in tables:
            self.cursor.execute(f"CREATE TABLE {table_name} {varlist}")

    def list_tables(self):
        self.cursor.execute("SELECT * FROM  SYSOBJECTS WHERE xtype = 'U'")
        return [x[0] for x in list(self.cursor)]

    def read_data(self, table_name, conditions=''):
        self.cursor.execute(f"SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=N'{table_name}'")
        varnames = [x[3] for x in list(self.cursor)]
        if len(conditions):
            cond_list = ' AND '.join([''.join([obs, ' = ', str(conditions[obs])]) for obs in conditions])
            self.cursor.execute(f"SELECT * FROM {table_name} WHERE {cond_list}")
        else:
            self.cursor.execute(f"SELECT * FROM {table_name}")
        in_list = [c for c in list(self.cursor)]
        data_list = [dict(zip(varnames, obs)) for obs in in_list]
        return data_list

class mysql(_sql_connector):
    def __init__(self,db_name):
        conn={}
        conn['user'] = 'incentive'
        conn['password'] = 'IncentiveDB1'
        conn['host'] = '34.72.6.132'
        self._conn = mc.connect(user=conn['user'], password=conn['password'], host=conn['host'])
        self._cursor = self._conn.cursor()
        self.cursor.execute('SHOW DATABASES')
        if db_name not in [x[0] for x in self.cursor]:
            self.cursor.execute(f'CREATE DATABASE {db_name}')
        self.cursor.execute(f'USE {db_name}')

def create_varlist(data_list):
    import pandas as pd
    import numpy as np
    df=pd.DataFrame(data_list)
    varlist = []
    for col in df.columns:
        if pd.api.types.is_datetime64_dtype(df[col]):
            varlist.append(f'{col} DATE')
        elif df[col].dtype == np.float64:
            varlist.append(f'{col} DOUBLE')
        elif df[col].dtype == np.int64:
            varlist.append(f'{col} INT')
        elif df[col].dtype == np.bool:
            varlist.append(f'{col} BOOL')
        else:
            max_length = df[col].astype(str).map(len).max()
            varlist.append(f'{col} VARCHAR({max_length})')
    varlist = ', '.join(varlist)
    varlist = f'({varlist})'
    return varlist