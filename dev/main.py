from datetime import datetime
import numpy as np
import pandas as pd
import copy
import math
import argparse
import incentivedkutils as utils
from gcp_sql import mysql
from gcp_sql import create_varlist
from exchange_rates import apply_exchange_rates
from exchange_rates import retrieve_exchange_rates
from cost_of_treatment import calculate_cost_of_treatment
from historical_data import load_historical_data
import sys
sys.path.append('/projects/primo_webscraper/test')
from prices_scraper import apply_price_formulas


def main():
    print('Write to live or test database?')
    database_type = input().lower() # test or live
    print()
    print(f'Writing to {database_type} database...')

    prices_exchanged = []
    output_prices_excel = []
    output_prices_tableau = []
    prices_historical, discontinued_products,  prices_manual, customer_access, product_details, \
        price_formulas, cost_of_treatment = load_master_tables()
    prices_manual = manual_prices_to_list_of_dicts(prices_historical, prices_manual)
    prices_scraped = scraped_prices_to_list_of_dicts()
    prices_new = prices_manual + prices_scraped

    for price_dict in prices_new:

        price_dict = transform_prices_to_float(price_dict)
        price_dict = replace_nans(price_dict)
        price_dict = add_product_details(price_dict, product_details)
        price_dict = apply_price_formulas(price_dict, price_formulas)
        price_dict = remove_superflous_prices(price_dict)
        price_dict = ucd_price_correction(price_dict)
        output_prices_excel.append(price_dict)

    write_data_excel(output_prices_excel, database_type)
    prices_excel = retrieve_prices_excel(database_type)

    exrates = retrieve_exchange_rates()

    for price_dict in prices_excel:
        prices_local, prices_EUR, prices_USD = apply_exchange_rates(price_dict, exrates)
        prices_exchanged.append(prices_local), prices_exchanged.append(prices_EUR), prices_exchanged.append(prices_USD)
    key_list = [key for key in prices_exchanged[0].keys() if key[0:5] == 'price']

    for key in key_list:
        prices_tableau = split_prices(prices_exchanged, key)
        output_prices_tableau = output_prices_tableau + prices_tableau

    discontinued_products_dict = write_historical_data_tableau(output_prices_tableau, discontinued_products, database_type)
    output_prices_tableau = keep_new_prices(output_prices_tableau, discontinued_products_dict)

    output_cost_of_treatment_tableau = calculate_cost_of_treatment(cost_of_treatment,output_prices_tableau,key_list)

    output_prices_tableau = add_listed_calculated_column(output_prices_tableau, price_formulas)
    write_price_data_tableau(output_prices_tableau, database_type)
    write_cost_of_treatment_data_tableau(output_cost_of_treatment_tableau, database_type)
    write_access(customer_access)
    print(f'Successfully wrote to {database_type} database.')


def add_listed_calculated_column(output_prices_tableau, price_formulas):


    for price in output_prices_tableau:
        match = 0
        for formula in price_formulas:
            if formula['product_type'] == None:
                formula['product_type'] = ''
            if price['country_name'] == formula['country_name'] and price['product_type'] == formula['product_type']:
                match = 1
                if formula[price['price_type'].lower().replace(' ','_')] == 'Input':
                    price['price_origin'] = 'Listed'
                else:
                    price['price_origin'] = 'Calculated'
        if match == 0:
            price['price_origin'] = 'Undefined'

    return output_prices_tableau

def keep_new_prices(output_prices_tableau, discontinued_products_dict):
    for price_dict in output_prices_tableau:
        price_dict['row_identifier'] = price_dict['product_id'] + '_' + price_dict['country_name'] + '_' + price_dict['manufacturer_global_name']
    output_prices_tableau = [price_dict for price_dict in output_prices_tableau if price_dict['row_identifier'] not in discontinued_products_dict.keys()]

    output_prices_tableau = pd.DataFrame(output_prices_tableau)
    output_prices_tableau['date'] = pd.to_datetime(output_prices_tableau['date'])
    output_prices_tableau = output_prices_tableau.sort_values('date', ascending=False).groupby(['product_id', 'country_name', 'manufacturer_global_name', 'price_type', 'product_currency'],as_index=False).first().drop(['row_identifier'], axis='columns').to_dict('records')
    return output_prices_tableau


def remove_superflous_prices(price_dict):
    superflous_prices = ['price_ex_factory_net', 'price_hospital_net', 'price_public_net_incl_vat', 'price_input']
    price_dict = {k: v for k, v in price_dict.items() if k not in superflous_prices}
    return price_dict


def split_prices(prices_exchanged, key):
    prices_dict = copy.deepcopy(prices_exchanged)
    for dict in prices_dict:
        new_dict = dict
        new_dict['price'] = dict[key]
        new_dict['price_type'] = ' '.join(key.split('_')).title()
    return prices_dict


def ucd_price_correction(price_dict):
    prices = [price for price in price_dict.keys() if price[0:5] == 'price']
    for price in prices:
        if price_dict['product_type'] == 'UCD' and price_dict['product_size_unit'] == 'tablets':
            price_dict[price] = price_dict[price] / float(price_dict['product_size'])
        elif price_dict['product_type'] == 'UCD' and price_dict['product_size_unit'] != 'tablets':
            price_dict[price] = price_dict[price] / float(price_dict['pack_details'].split(' ')[1])
        price_dict[price] = round(price_dict[price], 7)
    return price_dict


def manual_prices_to_list_of_dicts(prices_historical, prices_manual):
    prices_manual = pd.concat([prices_historical, prices_manual], axis=0, ignore_index=True).drop_duplicates()
    prices_manual = prices_manual.to_dict('records')
    for dict in prices_manual:
        dict['collection_method'] = 'Manual'
    return prices_manual

def scraped_prices_to_list_of_dicts():
    prices_scraped = mysql('primo').read_data(f'prices_scraped')
    for dict in prices_scraped:
        dict.update({'collection_method':'Scraped'})
        dict.pop('price_status')
        dict.pop('product_pack_size')
        dict.pop('price_input')
        # dict.pop('product_local_no')
        dict.pop('product_local_id')
        # dict.pop('product_local_descrip')
        dict.pop('product_description')
        dict.pop('product_classifier')
        dict.pop('price_timestamp')
        dict.pop('manufacturer_local_name')
        if dict['price_hospital'] == None:
            dict['price_hospital'] = 'nan'
    return prices_scraped


def add_product_details(price_dict, product_details):
    for details_dict in product_details:
        if (price_dict['product_id'], price_dict['country_name'], price_dict['manufacturer_global_name']) == \
                 (details_dict['product_id'], details_dict['country_name'], details_dict['manufacturer_global_name']):
            price_dict.update(details_dict)
    return price_dict


def transform_prices_to_float(price_dict):
    key_list = [key for key in price_dict.keys() if key[0:5] == 'price']
    for key in key_list:
        try:
            price_dict[key] = float(price_dict[key])
        except:
            price_dict[key] = price_dict[key]
    return price_dict


def write_data_excel(output_prices_excel, database_type):
    if len(output_prices_excel):
        output_df = pd.DataFrame(output_prices_excel)
        output_df = output_df.replace({None: ''})
        output_df = output_df.astype({'product_size': float, 'product_strength': float})
        output_prices = output_df.to_dict('Records')

        varlist = create_varlist(output_prices)
        mysql('primo').drop_table(f'prices_excel_{database_type}')
        mysql('primo').create_table(f'prices_excel_{database_type}', varlist)
        mysql('primo').insert_data(f'prices_excel_{database_type}', output_prices)
    return


def write_historical_data_tableau(output_prices_tableau, discontinued_products, database_type):
    discontinued_products['row_identifier'] = discontinued_products['product_id'] + '_' + discontinued_products['country'] + '_' + discontinued_products['manufacturer']
    discontinued_products_list = discontinued_products['row_identifier'].tolist()
    discontinue_date_list = discontinued_products['discontinue_date'].tolist()
    discontinued_products_dict = dict(zip(discontinued_products_list, discontinue_date_list))

    output_df = pd.DataFrame(output_prices_tableau).drop_duplicates()

    # adding current date
    output_df_copy = copy.deepcopy(output_df).drop_duplicates()
    output_df_copy['date'] = pd.to_datetime(output_df_copy.date)  # vi gør date til et datetimeobjekt, så pandas kan finde ud af at sortere det ordentligt
    output_df_copy['row_identifier'] = output_df_copy['product_id'] + '_' + output_df_copy['country_name'] + '_' + output_df_copy['manufacturer_global_name']
    output_df_copy = output_df_copy.sort_values('date', ascending=False).groupby(['product_id', 'country_name', 'manufacturer_global_name', 'price_type', 'product_currency'], as_index=False).first()  # .drop_duplicates()
    output_df_copy['date'] = datetime.today().strftime("%#m/%#d/%Y")

    # adding discontinue date
    for key in discontinued_products_dict.keys():
        indices = output_df_copy.index[output_df_copy['row_identifier'] == key].tolist()
        output_df_copy.iloc[indices, output_df_copy.columns.get_loc('date')] = discontinued_products_dict[key]

    # concatenating and converting to list of dicts
    output_df_copy = output_df_copy.drop(['row_identifier'], axis='columns')
    output_df = pd.concat([output_df, output_df_copy], axis=0, ignore_index=True).drop_duplicates()
    initial_prices_tableau = output_df.to_dict('Records')

    # keeping new prices
    varlist = create_varlist(initial_prices_tableau)
    mysql('primo').create_table(f'prices_tableau_historical_{database_type}', varlist)
    old_prices = mysql('primo').read_data(f'prices_tableau_historical_{database_type}')

    max_date = max([datetime.strptime(price['date'],"%m/%d/%Y") for price in old_prices])
    old_prices = [price for price in old_prices if datetime.strptime(price['date'],"%m/%d/%Y") != max_date]

    exdata_tuples = [(price['product_id'],price['country_name'],price['manufacturer_global_name'],price['date']) for price in old_prices]
    indata_tuples = [(price['product_id'],price['country_name'],price['manufacturer_global_name'],price['date']) for price in initial_prices_tableau]
    new_price_tuples = [tuple for tuple in indata_tuples if tuple not in exdata_tuples]
    new_prices = [price for price in initial_prices_tableau if (price['product_id'],price['country_name'],
                                                                          price['manufacturer_global_name'],price['date']) in new_price_tuples]
    output_prices_tableau = new_prices + old_prices

    # høker step, sætter alle 0 priser lig None, det burde være blevet håndteret i vores input
    # GCP-klassen sørger for at sætte None til NULL.
    for price_dict in output_prices_tableau:
        price_dict['price'] = zero_to_none(price_dict['price'])

    if len(new_prices) > 0:
        print()
        mysql('primo').drop_table(f'prices_tableau_historical_{database_type}')
        mysql('primo').create_table(f'prices_tableau_historical_{database_type}', varlist)
        mysql('primo').insert_data(f'prices_tableau_historical_{database_type}', output_prices_tableau)
        print(f'Wrote {int(len(new_price_tuples)/15)} new observations to historical {database_type} database.')
        print()
    return discontinued_products_dict


def zero_to_none(value):
    if value == 0:
        return None
    else:
        return value


def write_price_data_tableau(output_prices_tableau, database_type):
    for price_dict in output_prices_tableau:
        price_dict['price'] = zero_to_none(price_dict['price'])
    varlist = create_varlist(output_prices_tableau)
    mysql('primo').drop_table(f'prices_tableau_{database_type}')
    mysql('primo').create_table(f'prices_tableau_{database_type}', varlist)
    mysql('primo').insert_data(f'prices_tableau_{database_type}', output_prices_tableau)


def write_cost_of_treatment_data_tableau(output_cost_of_treatment_tableau, database_type):
    varlist = create_varlist(output_cost_of_treatment_tableau)
    mysql('primo').drop_table(f'cot_tableau_{database_type}')
    mysql('primo').create_table(f'cot_tableau_{database_type}', varlist)
    mysql('primo').insert_data(f'cot_tableau_{database_type}', output_cost_of_treatment_tableau)


def write_access(customer_access):
    varlist = create_varlist(customer_access)
    mysql('primo').drop_table('customer_access')
    mysql('primo').create_table('customer_access', varlist)
    mysql('primo').insert_data('customer_access', customer_access)
    return


def load_master_tables():
    customer_access = load_csv_utf8('/projects/primo_tableau/parms/customer_access.csv')
    product_details = load_csv_utf8('/projects/primo_tableau/parms/product_details.csv')
    cost_of_treatment = utils.load_csv_utf8('/projects/primo_tableau/parms/cost_of_treatment.csv')
    price_formulas = utils.load_csv_utf8('/projects/primo_webscraper/parms/price_formulas.csv')
    prices_manual = pd.read_csv('/projects/primo_tableau/parms/prices_manual.csv')
    discontinued_products = pd.read_csv('/projects/primo_tableau/parms/discontinued_products.csv')
    prices_historical = load_historical_data()
    return prices_historical, discontinued_products, prices_manual, customer_access, product_details, price_formulas, cost_of_treatment


def retrieve_prices_excel(database):
    prices_excel = mysql('primo').read_data(f'prices_excel_{database}')
    return prices_excel


def replace_nans(price_dict):
    price_dict = {k: replace_nan(k,v) for k, v in price_dict.items()}
    return price_dict


def replace_nan(k, v):
    if type(v) == float:
        if math.isnan(v):
            if 'product_type' in k:
                v = None
            else:
                v = 0
    return v


def isNaN(num):
    return num != num


def load_csv_utf8(file, delimiter=','):
    import csv
    with open(file, 'r', encoding='utf-8-sig') as infile:
        load_parms = [{k: empty_to_zero(v) for k, v in dict(obs).items()} for obs in csv.DictReader(infile, delimiter=delimiter)]
    return load_parms


def empty_to_zero(x):
    if x == 'nan':
        return 0
    if x == '1':
        return 1
    if x == np.nan:
        return 0
    if x == '':
        return None
    if x == '0':
        return 0
    else:
        return x


if __name__ == '__main__':
    main()
