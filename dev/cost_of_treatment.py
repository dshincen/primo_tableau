import sys
import pandas as pd
import incentivedkutils as utils

def main():
    cost_of_treatment = load_csv_utf8('/projects/primo_tableau/parms/country_specific.csv')
    return calculate_cost_of_treatment(cost_of_treatment)

def calculate_cost_of_treatment(cost_of_treatment,price_data, key_list):
    cot = pd.DataFrame(cost_of_treatment)
    price_data = pd.DataFrame(price_data)
    cot_df = cot.merge(price_data, how='left', left_on='product_local_name', right_on='product_local_name').replace({None: ""})
    cot_df['cot'] = (cot_df['price'].astype(float)*cot_df['dose_annual_treatment'].astype('float'))/(cot_df['product_strength'].astype('float')*cot_df['product_size'].astype('float'))
    cot_dicts = identify_min_max_pack_price(cot_df)
    output_dicts = add_custom_values(cot_dicts)
    return output_dicts

# funktionen bruges til at tilføje værdier til en wild card kolonne
# wild card kolonnen bruges til at lave overlappende bar charts i tableau ud fra kundens ønsker
def add_custom_values(cot_dicts):
    for dict in cot_dicts:
        dict['cot_match_column'] = dict['label_cot'].split(' ')[:-2]
        dict['cot_match_column'] = '_'.join(dict['cot_match_column']) + '_' + dict ['country_name'] + '_' + dict ['product_substance'] + '_' + dict['price_type'] + '_' + dict['product_currency']
        dict['reverse_cot_axis'] = 0
        dict['deletion_bool'] = 0
        dict['hide_bool'] = 0

    for dict in cot_dicts:
        if dict['product_local_name'] == 'Eylea' or dict['product_local_name'] == 'Lucentis':
            for match_dict in cot_dicts:
                if 'SPC' in dict['cot_match_column']:
                    if dict['cot_match_column'].replace('SPC', 'RWD') == match_dict['cot_match_column']:
                        dict['reverse_cot_axis'] = match_dict['cot']
                        if dict['cot'] < match_dict['cot']:
                            dict['deletion_bool'] = 0
                            dict['hide_bool'] = 1
                else:
                    if dict['cot_match_column'].replace('RWD', 'SPC') == match_dict['cot_match_column']:
                        dict['reverse_cot_axis'] = match_dict['cot']
                        if dict['cot'] < match_dict['cot']:
                            dict['deletion_bool'] = 0
                            dict['hide_bool'] = 1

        if dict['product_local_name'] == 'Beovu':
            for match_dict in cot_dicts:
                if 'No_disease_activity' in dict['cot_match_column']:
                    if dict['cot_match_column'].replace('No_disease_activity', 'Disease_activity') == match_dict['cot_match_column']:
                        dict['reverse_cot_axis'] = match_dict['cot']
                        if dict['cot'] < match_dict['cot']:
                            dict['deletion_bool'] = 0
                            dict['hide_bool'] = 1
                else:
                    if dict['cot_match_column'].replace('Disease_activity', 'No_disease_activity') == match_dict['cot_match_column']:
                        dict['reverse_cot_axis'] = match_dict['cot']
                        if dict['cot'] < match_dict['cot']:
                            dict['deletion_bool'] = 0
                            dict['hide_bool'] = 1
    cot_dicts = [dict for dict in cot_dicts if dict['deletion_bool']==0]

    df = pd.DataFrame(cot_dicts).drop(['cot_identifier','cot_max_min_identifier','cot_match_column', 'deletion_bool'], axis='columns')
    output_dicts = df.to_dict('Records')
    return output_dicts

def identify_min_max_pack_price(cot_df):
    cot_df['cot_identifier'] = cot_df['country_name'] + '_' + cot_df['product_substance'] + '_' + cot_df['label_cot'] + '_' + cot_df['price_type'] + '_' + cot_df['product_currency']
    cot_df['cot_max_min_identifier'] = cot_df['cot_identifier'] + '_' + cot_df['price_ex_factory'].astype(str)
    cot_df = cot_df.sort_values(['cot_max_min_identifier','price_type','cot_max_min_identifier','product_currency','price_type','country_name'])

    cot_dicts = cot_df.to_dict('Records')

    max_df = cot_df.groupby(['cot_identifier'], sort=True)['price_ex_factory'].max().reset_index()
    min_df = cot_df.groupby(['cot_identifier'], sort=True)['price_ex_factory'].min().reset_index()

    max_df['cot_max_min_identifier'] = max_df['cot_identifier'] + '_' + max_df['price_ex_factory'].astype(str)
    min_df['cot_max_min_identifier'] = min_df['cot_identifier'] + '_' + min_df['price_ex_factory'].astype(str)

    max_list = max_df['cot_max_min_identifier'].tolist()
    min_list = min_df['cot_max_min_identifier'].tolist()

    c = 0
    for dict in cot_dicts:
        if c == 0:
            previous_identifier = dict['cot_max_min_identifier']
            dict['max'] = 'Yes' if dict['cot_max_min_identifier'] in max_list else 'No'
            dict['min'] = 'Yes' if dict['cot_max_min_identifier'] in min_list else 'No'
        elif dict['cot_max_min_identifier'] == previous_identifier:
            dict['max'] = 'No'
            dict['min'] = 'No'
        else:
            dict['max'] = 'Yes' if dict['cot_max_min_identifier'] in max_list else 'No'
            dict['min'] = 'Yes' if dict['cot_max_min_identifier'] in min_list else 'No'
            previous_identifier = dict['cot_max_min_identifier']
        c+=1
    return cot_dicts

def load_csv_utf8(file, delimiter=','):
    import csv
    with open(file, 'r',encoding='utf-8-sig') as infile:
        load_parms = [{k: empty_to_zero(v) for k, v in dict(obs).items()} for obs in csv.DictReader(infile,delimiter = delimiter)]
    return load_parms

def empty_to_zero(x):
    if x=='nan':
        return 0
    if x == '1':
        return 1
    if x == '':
        return None
    if x == '0':
        return 0
    else:
        return x

if __name__ == '__main__':
    main()
