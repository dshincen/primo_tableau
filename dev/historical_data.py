from datetime import datetime, timedelta
import sys
import xlrd
import bs4
import numpy as np
import pandas as pd
import copy
import pandas as pd
import glob
from gcp_sql import mysql

def load_historical_data():
    load_past = False
    if load_past:
        list_of_dfs = []
        path = r'C:\projects\primo_tableau\parms\prices_historical'
        all_files = glob.glob(path + "/*.csv")

        for filename in all_files:
            df = pd.read_csv(filename, index_col=None, header=0)
            list_of_dfs.append(df)
        historical_df = pd.concat(list_of_dfs, axis=0, ignore_index=True)
    else:
        historical_df = pd.read_csv('/projects/primo_tableau/parms/prices_manual.csv') # loading only current data
    return historical_df

def load_csv_utf8(file, delimiter=','):
    import csv
    with open(file, 'r',encoding='utf-8-sig') as infile:
        load_parms = [{k: empty_to_zero(v) for k, v in dict(obs).items()} for obs in csv.DictReader(infile,delimiter = delimiter)]
    return load_parms

def empty_to_zero(x):
    if x=='nan':
        return 0
    if x == '1':
        return 1
    if x == '':
        return None
    if x == '0':
        return 0
    else:
        return x

if __name__ == '__main__':
    main()
