def apply_price_formula(std_input_dict, price_formulas):
    for price in price_formulas:
        if price_formulas[price][0] == 'Estimated':
            price_formulas[price] += [0] * 32

    # find input prices and estimated prices
    input_prices = [price for price in price_formulas if
                    (price_formulas[price][0] == 'Input' and price in std_input_dict)]
    estimated_prices = [price for price in price_formulas if price not in input_prices]
    std_input_dict['price_input'] = input_prices

    # calculating prices
    for price in estimated_prices:
        price_base = price_formulas[price][1]
        price_parms = [float(p) for p in price_formulas[price][2:]]
        if price_parms[2] == 0: price_parms[2] = 9999999999
        if price_parms[5] == 0: price_parms[5] = price_parms[2] + 9999999999
        if price_parms[8] == 0: price_parms[8] = price_parms[5] + 9999999999
        if price_parms[11] == 0: price_parms[11] = price_parms[8] + 9999999999
        if price_parms[14] == 0: price_parms[14] = price_parms[11] + 9999999999
        if price_parms[17] == 0: price_parms[17] = price_parms[14] + 9999999999
        if price_parms[20] == 0: price_parms[20] = price_parms[17] + 9999999999
        if price_parms[23] == 0: price_parms[23] = price_parms[20] + 9999999999
        if price_parms[26] == 0: price_parms[26] = price_parms[23] + 9999999999
        if price_parms[29] == 0: price_parms[29] = price_parms[26] + 9999999999

        # if upper lower limit of price cases is 0 (price cases does not exist) or upper lower limit is larger than input price then use these price parameters
        if price_parms[2] > std_input_dict[price_base]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[0] + price_parms[1]
        # else if input price is larger than upper lower limit but less than or equal to upper middle limit use these price parameters
        elif price_parms[2] <= std_input_dict[price_base] < price_parms[5]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[3] + price_parms[4]
        elif price_parms[5] < std_input_dict[price_base] < price_parms[8]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[6] + price_parms[7]
        elif price_parms[8] < std_input_dict[price_base] < price_parms[11]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[9] + price_parms[10]
        elif price_parms[11] < std_input_dict[price_base] < price_parms[14]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[12] + price_parms[13]
        elif price_parms[14] < std_input_dict[price_base] < price_parms[17]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[15] + price_parms[16]
        elif price_parms[17] < std_input_dict[price_base] < price_parms[20]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[18] + price_parms[19]
        elif price_parms[20] < std_input_dict[price_base] < price_parms[23]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[21] + price_parms[22]
        elif price_parms[23] < std_input_dict[price_base] < price_parms[26]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[24] + price_parms[25]
        elif price_parms[26] < std_input_dict[price_base] < price_parms[29]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[27] + price_parms[28]
        elif price_parms[29] < std_input_dict[price_base]:
            std_input_dict[price] = std_input_dict[price_base] * price_parms[30] + price_parms[31]
    # op til 11

    # correcting ucd prices
    ucd_price_correction(std_input_dict, input_prices, estimated_prices)
    return std_input_dict

def ucd_price_correction(std_input_dict, input_prices, estimated_prices):
    for price in input_prices + estimated_prices:
        if std_input_dict['product_type'] == 'UCD' and std_input_dict['product_size_unit'] == 'tablets':
            std_input_dict[price] = std_input_dict[price] *  float(std_input_dict['product_size'])
        elif std_input_dict['product_type'] == 'UCD' and std_input_dict['product_size_unit'] != 'tablets':
            try:
                std_input_dict[price] = std_input_dict[price] * float(std_input_dict['product_description'][0])
            except:
                std_input_dict[price] = std_input_dict[price] * float(std_input_dict['pack_details'].split(' ')[1])
        std_input_dict[price] = round(std_input_dict[price], 7)