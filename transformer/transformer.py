from datetime import datetime, timedelta
from dev.gcp_sql import mysql
from dev.gcp_sql import create_varlist
import sys
sys.path.append('/projects/primo_webscraper/test')
from price_formula import apply_price_formula

class Transformer():

    def __init__(self, database_type):
        self.connection = mysql('primo')
        self.database_type = database_type


    def change_product_type(self, product_id, country_name, manufacturer_global_name, date_start, date_end, new_product_type):
        historical_records = self.connection.read_data(f'prices_tableau_historical_{self.database_type}')
        comparison_tuple = (product_id, country_name, manufacturer_global_name)
        for record in historical_records:
            record_tuple = (record['product_id'], record['country_name'], record['manufacturer_global_name'])
            if record_tuple == comparison_tuple:
                if datetime.strptime(date_end,'%m/%d/%Y') >= datetime.strptime(record['date'],'%m/%d/%Y') >= datetime.strptime(date_start,'%m/%d/%Y'):
                    record['product_type'] = new_product_type
        self.write_to_db(historical_records)


    def apply_price_formula(self, product_id, country_name, manufacturer_global_name, date_start, date_end, price_formula):
        historical_records = self.connection.read_data(f'prices_tableau_historical_{self.database_type}')
        comparison_tuple = (product_id, country_name, manufacturer_global_name)
        for record in historical_records:
            record_tuple = (record['product_id'], record['country_name'], record['manufacturer_global_name'])
            if record_tuple == comparison_tuple:
                if datetime.strptime(date_end, '%m/%d/%Y') >= datetime.strptime(record['date'], '%m/%d/%Y') >= datetime.strptime(date_start, '%m/%d/%Y'):
                    print('Record before price change:')
                    print(record)
                    record = apply_price_formula(record, price_formula)
                    record['price'] = record['_'.join(record['price_type'].lower().split(' '))]
                    print('Record after price change:')
                    print(record)
                    print()
                    record.pop('price_input')
        self.write_to_db(historical_records)


    def change_variable(self, product_id, country_name, manufacturer_global_name, date_start, date_end, key, value):
        historical_records = self.connection.read_data(f'prices_tableau_historical_{self.database_type}')
        comparison_tuple = (product_id, country_name, manufacturer_global_name)
        for record in historical_records:
            record_tuple = (record['product_id'], record['country_name'], record['manufacturer_global_name'])
            if record_tuple == comparison_tuple:
                if datetime.strptime(date_end, '%m/%d/%Y') >= datetime.strptime(record['date'], '%m/%d/%Y') >= datetime.strptime(date_start, '%m/%d/%Y'):
                    print('Found record to change.')
                    record[key] = value
                    record['price'] = record['_'.join(record['price_type'].lower().split(' '))]
                    print('Changed record.')
                    print()
        self.write_to_db(historical_records)


    def delete_records(self, product_id, country_name, manufacturer_global_name, date_start, date_end):
        historical_records = self.connection.read_data(f'prices_tableau_historical_{self.database_type}')
        comparison_tuple = (product_id, country_name, manufacturer_global_name)
        list_of_records_to_delete = []
        for record in historical_records:
            record_tuple = (record['product_id'], record['country_name'], record['manufacturer_global_name'])
            if record_tuple == comparison_tuple:
                if datetime.strptime(date_end, '%m/%d/%Y') >= datetime.strptime(record['date'], '%m/%d/%Y') >= datetime.strptime(date_start, '%m/%d/%Y'):
                    print('Found record to delete...')
                    try:
                        list_of_records_to_delete.append(record)
                    except:
                        print('Error')
                        print()
        try:
            historical_records = [record for record in historical_records if record not in list_of_records_to_delete]
            if len(list_of_records_to_delete) > 0:
                self.write_to_db(historical_records)
                print(f'Successfully deleted {len(list_of_records_to_delete)} records.')
                print()
            if len(list_of_records_to_delete) == 0:
                print('No records to delete.')
                print()
        except:
            print('Delete failed.')

    def write_to_db(self, records):
        varlist = create_varlist(records)
        self.connection.drop_table(f'prices_tableau_historical_{self.database_type}')
        self.connection.create_table(f'prices_tableau_historical_{self.database_type}', varlist)
        self.connection.insert_data(f'prices_tableau_historical_{self.database_type}', records)


    def backup_db(self):
        return 2


if __name__ == '__main__':
    main()
