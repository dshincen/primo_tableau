from transformer import Transformer

def main():

    # instantiate using test or live database
    db_transformer = Transformer('test')

    # backup database

    # change product type
    #db_transformer.change_product_type('ravulizumab_vial_300_3_1_injection','United States',
    #                                   'Alexion Pharmaceuticals', '07/23/1900', '12/23/2020', 'deviating_soliris')

    # change price formula
    #price_formula = {'price_ex_factory': ['Input'], 'price_pharmacy': ['Estimated', 'price_ex_factory', '1.08'],
    # 'price_public_ex_vat': ['Estimated', 'price_ex_factory', '1.3392'],
    # 'price_public_incl_vat': ['Estimated', 'price_ex_factory', '1.3392']}

    #db_transformer.apply_price_formula('hepatitis_b_immunoglobulin_vial_2000_40_1_infusion', 'South Africa', 'Biotest',
    #                                   '07/23/1990','08/15/2021', price_formula)

    # change a value for a given key
    db_transformer.change_variable('ravulizumab_vial_300_3_1_injection','United States',
                                       'Alexion Pharmaceuticals', '07/23/1900', '12/23/2020','manufacturer_global_name', 'Dittes Pharma')

    # delete records
    db_transformer.delete_records('ravulizumab_vial_300_30_1_injection','Iceland',
                                       'Alexion Pharmaceuticals', '07/21/2026', '07/21/2026')

    db_transformer.delete_records('ravulizumab_vial_300_30_1_injection','United Arab Emirates',
                                       'Alexion Pharmaceuticals', '05/25/2023', '05/25/2023')

if __name__ == '__main__':
    main()
