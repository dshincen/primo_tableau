from transformer import Transformer

def main():

    # instantiate using test or live database
    db_transformer = Transformer('test')

    # 08/16/2021 changed product type from soliris to deviating_soliris
    # db_transformer.change_product_type('ravulizumab_vial_300_3_1_injection','United States',
    #                                   'Alexion Pharmaceuticals', '07/23/1900', '12/23/2020', 'deviating_soliris')

    # change price formula in Cyprus for all products from 1. May 2021
    # price_formula = {'price_ex_factory': ['Estimated', 'price_public_incl_vat', '0.630865', '0', '13.7', '0.640212', '0', '68.5', '0.649839', '0', '332.5', '0.864286', '-75.3225', '1583', '0.864286', '-90.75'],
    # 'price_pharmacy': ['Estimated', 'price_public_incl_vat', '0.69516858', '0', '13.70', '0.705467', '0', '68.5', '0.716076', '0', '332.5', '0.952381', '-83', '1583', '0.952381', '-100'],
    # 'price_public_ex_vat': ['Estimated', 'price_public_incl_vat', '0.95238095'],
    # 'price_public_incl_vat': ['Input']}

    # db_transformer.apply_price_formula('human_albumin_vial_10000_50_1_infusion', 'Cyprus', 'Biotest',
    #                                  '05/01/2021', '09/03/2021', price_formula)

    # db_transformer.apply_price_formula('human_albumin_vial_20000_100_1_infusion', 'Cyprus', 'Biotest',
    #                                  '05/01/2021', '09/03/2021', price_formula)

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(ivig)_vial_5000_50_1_infusion', 'Cyprus', 'Biotest',
    #                                  '05/01/2021','09/03/2021', price_formula)

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(ivig)_vial_10000_100_1_infusion', 'Cyprus', 'Biotest',
    #                                  '05/01/2021','09/03/2021', price_formula)

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(ivig)_vial_5000_100_1_infusion', 'Cyprus', 'Biotest',
    #                                  '05/01/2021', '09/03/2021', price_formula)

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(ivig)_vial_2500_50_1_infusion', 'Cyprus', 'Biotest',
    #                                   '05/01/2021', '09/03/2021', price_formula)

    # db_transformer.apply_price_formula('brolucizumab_prefilled_syringe_6_0.05_1_injection', 'Cyprus', 'Biotest',
    #                                  '05/01/2021', '09/03/2021', price_formula)

    # db_transformer.apply_price_formula('aflibercept_vial_2_0.05_1_injection', 'Cyprus', 'Biotest',
    #                                   '05/01/2021', '09/03/2021', price_formula)

    # db_transformer.apply_price_formula('aflibercept_prefilled_syringe_2_0.165_1_injection', 'Cyprus', 'Biotest',
    #                                   '05/01/2021', '09/03/2021', price_formula)

    # db_transformer.apply_price_formula('ranibizumab_vial_0.5_0.23_1_injection', 'Cyprus', 'Biotest',
    #                                   '05/01/2021', '09/03/2021', price_formula)

    # db_transformer.apply_price_formula('ranibizumab_prefilled_syringe_0.5_0.165_1_injection', 'Cyprus', 'Biotest',
    #                                  '05/01/2021', '09/03/2021', price_formula)

    # Change ex-factory input price for Cytotect in Germany from 1 January 2021
    # db_transformer.change_variable('human_cytomegalovirus_immunoglobulin_vial_5000_50_1_infusion', 'Germany', 'Biotest',
    #                               '01/01/2021', '09/08/2021', 'price_ex_factory', 889.9)

    # db_transformer.change_variable('human_cytomegalovirus_immunoglobulin_vial_1000_10_1_infusion', 'Germany', 'Biotest',
    #                               '01/01/2021', '09/08/2021', 'price_ex_factory', 183.2)

    # Change hospital input price for Cytotect in Germany from 1 January 2021
    # db_transformer.change_variable('human_cytomegalovirus_immunoglobulin_vial_5000_50_1_infusion', 'Germany', 'Biotest',
    #                               '01/01/2021', '09/08/2021', 'price_hospital', 889.9)

    # db_transformer.change_variable('human_cytomegalovirus_immunoglobulin_vial_1000_10_1_infusion', 'Germany', 'Biotest',
    #                               '01/01/2021', '09/08/2021', 'price_hospital', 183.2)

    # Change pharmacy input price for Cytotect in Germany from 1 January 2021
    # db_transformer.change_variable('human_cytomegalovirus_immunoglobulin_vial_5000_50_1_infusion', 'Germany', 'Biotest',
    #                               '01/01/2021', '09/08/2021', 'price_pharmacy', 918.63)

    # db_transformer.change_variable('human_cytomegalovirus_immunoglobulin_vial_1000_10_1_infusion', 'Germany', 'Biotest',
    #                               '01/01/2021', '09/08/2021', 'price_pharmacy', 189.67)


    # Change price_public_incl_vat price for Hepatect in Argentina from 1 January 2018
    # db_transformer.change_variable('hepatitis_b_immunoglobulin_vial_500_10_1_infusion', 'Argentina', 'Biotest',
    #                              '01/01/2018', '09/10/2021', 'price_public_incl_vat', 8283.27)

    # Change price_public_incl_vat price for Intratect in Argentina from 3 November 2020
    # db_transformer.change_variable('human_normal_immunoglobulin_(ivig)_vial_5000_100_1_infusion', 'Argentina', 'Biotest',
    #                                '11/03/2020', '04/30/2021', 'price_public_incl_vat', 222020.31)

    # db_transformer.change_variable('human_normal_immunoglobulin_(ivig)_vial_5000_100_1_infusion', 'Argentina', 'Biotest',
    #                                '05/01/2021', '06/30/2021', 'price_public_incl_vat', 256992.95)

    # db_transformer.change_variable('human_normal_immunoglobulin_(ivig)_vial_5000_100_1_infusion', 'Argentina', 'Biotest',
    #                               '07/01/2021', '09/01/2021', 'price_public_incl_vat', 277552.38)

    # db_transformer.change_variable('human_normal_immunoglobulin_(ivig)_vial_5000_100_1_infusion', 'Argentina', 'Biotest',
    #                               '09/02/2021', '09/30/2021', 'price_public_incl_vat', 288654.48)

    # Change price_public_incl_vat price for Pentaglobin in Argentina from 3 November 2020
    #
    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'Argentina', 'Biotest',
    #                                '11/03/2020', '04/30/2021', 'price_public_incl_vat', 115247.33)

    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'Argentina', 'Biotest',
    #                                '05/01/2021', '06/30/2021', 'price_public_incl_vat', 133401.09)

    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'Argentina', 'Biotest',
    #                                '07/01/2021', '09/01/2021', 'price_public_incl_vat', 144073.18)

    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'Argentina', 'Biotest',
    #                                '09/02/2021', '09/30/2021', 'price_public_incl_vat', 149836.11)


    # Change price formula in Argentina for all historical prices
    # price_formula = {'price_ex_factory': ['Estimated', 'price_public_incl_vat', '0.5699'],
    # 'price_pharmacy': ['Estimated', 'price_public_incl_vat', '0.61983471'],
    # 'price_public_ex_vat': ['Estimated', 'price_public_incl_vat', '0.82644628'],
    # 'price_public_incl_vat': ['Input']}

    # db_transformer.apply_price_formula('hepatitis_b_immunoglobulin_vial_500_10_1_infusion', 'Argentina', 'Biotest',
    #                                 '01/01/2018', '09/10/2021', price_formula)

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(ivig)_vial_5000_100_1_infusion', 'Argentina', 'Biotest',
    #                                 '11/03/2020', '09/10/2021', price_formula)

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'Argentina', 'Biotest',
    #                                 '11/03/2020', '09/10/2021', price_formula)


    # Change price_pharmacy for Pentaglobin in South Africa from 11 March 2020
    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'South Africa', 'Biotest',
    #                               '03/11/2020', '09/15/2021', 'price_pharmacy', 3172.91176756066)

    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_5000_100_1_infusion', 'South Africa', 'Biotest',
    #                               '03/11/2020', '09/15/2021', 'price_pharmacy', 5668.0739550194)

    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_500_10_1_infusion', 'South Africa', 'Biotest',
    #                               '03/11/2020', '09/15/2021', 'price_pharmacy', 566.816183349744)

    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_1000_20_1_infusion', 'South Africa', 'Biotest',
    #                               '03/11/2020', '09/15/2021', 'price_pharmacy', 1133.63236669949)


    # Change price formula for Pentaglobin 50 ml in South Africa from 11 March 2020
    # price_formula = {'price_ex_factory': ['Input'],
    # 'price_pharmacy': ['Input'],
    # 'price_public_ex_vat': ['Estimated', 'price_pharmacy', '1', '373.1224'],
    # 'price_public_incl_vat': ['Estimated', 'price_pharmacy', '1.15', '429.0908']}

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'South Africa', 'Biotest',
    #                                   '03/11/2020','09/15/2021', price_formula)

    # Change price formula for Pentaglobin 100 ml in South Africa from 11 March 2020
    # price_formula = {'price_ex_factory': ['Input'],
    # 'price_pharmacy': ['Input'],
    # 'price_public_ex_vat': ['Estimated', 'price_pharmacy', '1', '516.5943'],
    # 'price_public_incl_vat': ['Estimated', 'price_pharmacy', '1.15', '594.0834']}

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(igm)_vial_5000_100_1_infusion', 'South Africa', 'Biotest',
    #                                   '03/11/2020', '09/15/2021', price_formula)

    # Change price formula for Pentaglobin 10 ml in South Africa from 11 March 2020
    # price_formula = {'price_ex_factory': ['Input'],
    # 'price_pharmacy': ['Input'],
    # 'price_public_ex_vat': ['Estimated', 'price_pharmacy', '1', '180.5458'],
    # 'price_public_incl_vat': ['Estimated', 'price_pharmacy', '1.15', '207.6277']}

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(igm)_vial_500_10_1_infusion', 'South Africa', 'Biotest',
    #                                    '03/11/2020', '09/15/2021', price_formula)

    # Change price formula for Pentaglobin 20 ml in South Africa from 11 March 2020
    # price_formula = {'price_ex_factory': ['Input'],
    # 'price_pharmacy': ['Input'],
    # 'price_public_ex_vat': ['Estimated', 'price_pharmacy', '1', '255.8639'],
    # 'price_public_incl_vat': ['Estimated', 'price_pharmacy', '1.15', '294.2434']}

    # db_transformer.apply_price_formula('human_normal_immunoglobulin_(igm)_vial_1000_20_1_infusion', 'South Africa', 'Biotest',
    #                                    '03/11/2020', '09/15/2021', price_formula)

    # United States, Lucentis: Change price_public_incl_vat as the wrong price has been listed
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.05_1_injection', 'United States', 'Novartis',
    #                                '01/30/2017', '09/30/2021', 'price_public_incl_vat', 2340)
    # United States, Lucentis: Change price_public_ex_vat as the wrong price has been calculated
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.05_1_injection', 'United States', 'Novartis',
    #                                '01/30/2017', '09/30/2021', 'price_public_ex_vat', 2127.27)

    # Lebanon, Eylea vial: Change prices for all price levels as wrong calculations have been applied since 2 March 2021
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '03/02/2021', '09/30/2021', 'price_pharmacy', 1302046.29)
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '03/02/2021', '09/30/2021', 'price_ex_factory', 1183678.45)
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '03/02/2021', '09/30/2021', 'price_public_ex_vat', 1302057)
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '03/02/2021', '09/30/2021', 'price_public_incl_vat', 1302057)

    # Lebanon, Eylea prefilled syringe: Change prices for all price levels as wrong calculations have been applied since 3 June 2021
    # db_transformer.change_variable('aflibercept_prefilled_syringe_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '06/03/2021', '09/30/2021', 'price_pharmacy', 1370583.29)
    # db_transformer.change_variable('aflibercept_prefilled_syringe_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '06/03/2021', '09/30/2021', 'price_ex_factory', 1245984.81)
    # db_transformer.change_variable('aflibercept_prefilled_syringe_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '06/03/2021', '09/30/2021', 'price_public_ex_vat', 1370594)
    # db_transformer.change_variable('aflibercept_prefilled_syringe_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                               '06/03/2021', '09/30/2021', 'price_public_incl_vat', 1370594)

    # Lebanon, Lucentis: Change prices for all price levels as wrong calculations have been applied since 2 september 2020
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.23_1_injection', 'Lebanon', 'Novartis',
    #                                '09/02/2020', '09/30/2021', 'price_ex_factory', 1175653.9)
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.23_1_injection', 'Lebanon', 'Novartis',
    #                                '09/02/2020', '09/30/2021', 'price_pharmacy', 1293219.29)
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.23_1_injection', 'Lebanon', 'Novartis',
    #                                '09/02/2020', '09/30/2021', 'price_public_ex_vat', 1293230)
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.23_1_injection', 'Lebanon', 'Novartis',
    #                                '09/02/2020', '09/30/2021', 'price_public_incl_vat', 1293230)

    # Lebanon, Beovu: Change prices for all price levels as wrong calculations have been applied since 7 April 2021
    # db_transformer.change_variable('brolucizumab_prefilled_syringe_6_0.05_1_injection', 'Lebanon', 'Novartis',
    #                                '04/07/2021', '09/30/2021', 'price_ex_factory', 1074011.1727)
    # db_transformer.change_variable('brolucizumab_prefilled_syringe_6_0.05_1_injection', 'Lebanon', 'Novartis',
    #                                '04/07/2021', '09/30/2021', 'price_pharmacy', 1181412.29)
    # db_transformer.change_variable('brolucizumab_prefilled_syringe_6_0.05_1_injection', 'Lebanon', 'Novartis',
    #                                '04/07/2021', '09/30/2021', 'price_public_ex_vat', 1181423)
    # db_transformer.change_variable('brolucizumab_prefilled_syringe_6_0.05_1_injection', 'Lebanon', 'Novartis',
    #                                '04/07/2021', '09/30/2021', 'price_public_incl_vat', 1181423)


    # Lebanon, Eylea vial, Eylea prefilled syringe, Lucentis, Beovu: Add price_hospital as this price level is missing
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '09/02/2020', '03/01/2021', 'price_hospital', 1375698)
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '03/02/2021', '09/30/2021', 'price_hospital', 1406221)
    # db_transformer.change_variable('aflibercept_prefilled_syringe_2_0.1_1_injection', 'Lebanon', 'Bayer',
    #                                '06/03/2021', '09/02/2021', 'price_hospital', 1406221)
    # db_transformer.change_variable('brolucizumab_prefilled_syringe_6_0.05_1_injection', 'Lebanon', 'Novartis',
    #                               '04/07/2021', '09/02/2021', 'price_hospital', 1396689)
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.23_1_injection', 'Lebanon', 'Novartis',
    #                               '09/02/2020', '09/30/2021', 'price_hospital', 1396689)

    # Latvia, Eylea: Change prices for all price levels as wrong calculations have been applied since 1. December 2020
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Latvia', 'Bayer',
    #                                '12/01/2020', '09/15/2021', 'price_ex_factory', 814.00)
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Latvia', 'Bayer',
    #                                '12/01/2020', '09/15/2021', 'price_pharmacy', 830.28)
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Latvia', 'Bayer',
    #                                '12/01/2020', '09/15/2021', 'price_public_ex_vat', 836.33)
    # db_transformer.change_variable('aflibercept_vial_2_0.1_1_injection', 'Latvia', 'Bayer',
    #                                '12/01/2020', '09/15/2021', 'price_public_incl_vat', 936.69)

    #Latvia, Lucentis: Change prices for all price levels as wrong calculations have been applied since 1. December 2020
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.23_1_injection', 'Latvia', 'Novartis',
    #                               '12/01/2020', '09/15/2021', 'price_ex_factory', 935.00)
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.23_1_injection', 'Latvia', 'Novartis',
    #                               '12/01/2020', '09/15/2021', 'price_pharmacy', 953.70)
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.23_1_injection', 'Latvia', 'Novartis',
    #                               '12/01/2020', '09/15/2021', 'price_public_ex_vat', 959.75)
    # db_transformer.change_variable('ranibizumab_vial_0.5_0.23_1_injection', 'Latvia', 'Novartis',
    #                               '12/01/2020', '09/15/2021', 'price_public_incl_vat', 1074.92)

    # Latvia, Soliris: Change prices for all price levels as wrong calculations have been applied since 1. October 2019
    # db_transformer.change_variable('eculizumab_vial_300_30_1_injection', 'Latvia', 'Alexion Pharmaceuticals',
    #                               '10/01/2019', '09/15/2021', 'price_ex_factory', 5063.00)
    # db_transformer.change_variable('eculizumab_vial_300_30_1_injection', 'Latvia', 'Alexion Pharmaceuticals',
    #                               '10/01/2019', '09/15/2021', 'price_pharmacy', 5113.63)
    # db_transformer.change_variable('eculizumab_vial_300_30_1_injection', 'Latvia', 'Alexion Pharmaceuticals',
    #                               '10/01/2019', '09/15/2021', 'price_public_ex_vat', 5119.68)
    # db_transformer.change_variable('eculizumab_vial_300_30_1_injection', 'Latvia', 'Alexion Pharmaceuticals',
    #                               '10/01/2019', '09/15/2021', 'price_public_incl_vat', 5734.04)

    # Argentina, Intratect 5%: Change prices for all price levels from 2 September 2021 until 30 September 2021
    # db_transformer.change_variable('human_normal_immunoglobulin_(ivig)_vial_5000_100_1_infusion', 'Argentina', 'Biotest',
    #                              '09/02/2021', '09/30/2021', 'price_ex_factory', 164504.188)
    # db_transformer.change_variable('human_normal_immunoglobulin_(ivig)_vial_5000_100_1_infusion', 'Argentina', 'Biotest',
    #                               '09/02/2021', '09/30/2021', 'price_pharmacy', 178918.066)
    # db_transformer.change_variable('human_normal_immunoglobulin_(ivig)_vial_5000_100_1_infusion', 'Argentina', 'Biotest',
    #                                '09/02/2021', '09/30/2021', 'price_public_ex_vat', 238557.421)

    # Argentina, Pentaglobin: Change prices for all price levels from 2 September 2021 until 30 September 2021
    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'Argentina', 'Biotest',
    #                                '09/02/2021', '09/30/2021', 'price_ex_factory', 85391.599)
    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'Argentina', 'Biotest',
    #                                '09/02/2021', '09/30/2021', 'price_pharmacy', 92873.621)
    # db_transformer.change_variable('human_normal_immunoglobulin_(igm)_vial_2500_50_1_infusion', 'Argentina', 'Biotest',
    #                               '09/02/2021', '09/30/2021', 'price_public_ex_vat', 123831.496)

    # Poland, Soliris: Pharmacy price added since this price level was missing
    # db_transformer.change_variable('eculizumab_vial_300_30_1_injection', 'Poland', 'Alexion Pharmaceuticals',
    #                               '11/01/2019', '10/19/2021', 'price_pharmacy', 17851.0462962963)

if __name__ == '__main__':
    main()